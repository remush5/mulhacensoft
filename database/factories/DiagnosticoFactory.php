<?php

namespace Database\Factories;

use App\Models\Diagnostico;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiagnosticoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Diagnostico::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'descripcion' => 'testadescripcion',
            'slug' => md5(uniqid(rand(), true)),
            'paciente_slug' => Paciente::where('user_id', 1)->first()->slug,
        ];
    }
}
