<?php

namespace Tests\Feature;

use App\Models\Diagnostico;
use App\Models\Paciente;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PacienteUsoTest extends TestCase
{
    /** @test */
    public function create_paciente()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $response = $this->post('/pacientes', [
            'nombres' => 'creatednombres',
            'apellidos' => 'createdapellidos',
            'dni' => '52357826M',
        ]);

        $response->assertOk();

        $paciente = Paciente::latest('created_at')->first();

        $this->assertEquals($paciente->nombres, 'creatednombres');
        $this->assertEquals($paciente->apellidos, 'createdapellidos');
        $this->assertEquals($paciente->dni, '52357826M');
        $this->assertEquals($paciente->user_id, $user->id);
    }

    /** @test */
    public function update_paciente()
    {
        $this->withoutExceptionHandling();

        $paciente = Paciente::latest('created_at')->first();

        $response = $this->put('/pacientes/'. $paciente->slug, [
            'nombres' => 'updatednombres',
            'apellidos' => 'updatedapellidos',
            'dni' => '52357826M',
        ]);

        $response->assertOk();

        $paciente_updated = Paciente::latest('updated_at')->first();

        $this->assertEquals($paciente_updated->nombres, 'updatednombres');
        $this->assertEquals($paciente_updated->apellidos, 'updatedapellidos');
        $this->assertEquals($paciente_updated->dni, '52357826M');
        $this->assertEquals($paciente_updated->slug, $paciente->slug);
    }

    /** @test */
    public function delete_paciente()
    {
        $this->withoutExceptionHandling();

        $paciente = Paciente::latest('created_at')->first();

        $response = $this->delete('/pacientes/'. $paciente->slug);

        $response->assertOk();

        $paciente_deleted = Paciente::where('slug', $paciente->slug)->first();

        $this->assertEmpty($paciente_deleted);
    }

    /** @test */
    public function get_pacientes_view()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        Paciente::factory()->count(3)->create();

        $response = $this->get('/home');

        $response->assertOk();

        $pacientes = Paciente::where('user_id', $user->id)->get();

        $response->assertViewIs('home');
        $response->assertViewHas('pacientes', $pacientes);
    }

    /** @test */
    public function get_paciente_view()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        $paciente = Paciente::factory()->create();
        Diagnostico::factory()->count(3)->create();

        $response = $this->get('/pacientes/'.$paciente->slug);

        $response->assertOk();

        $diagnosticos = Diagnostico::where('paciente_slug', $paciente->slug)->get();

        $response->assertViewIs('paciente');
        $response->assertViewHas('diagnosticos', $diagnosticos);
    }

    /** @test */
    public function update_api_paciente()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $paciente = Paciente::latest('created_at')->first();

        $response = $this->put('/api/pacientes/'. $paciente->slug, [
            'nombres' => 'updatednombres',
            'apellidos' => 'updatedapellidos',
            'dni' => '52357826M',
        ]);

        $response->assertOk();

        $paciente_updated = Paciente::latest('updated_at')->first();

        $this->assertEquals($paciente_updated->nombres, 'updatednombres');
        $this->assertEquals($paciente_updated->apellidos, 'updatedapellidos');
        $this->assertEquals($paciente_updated->dni, '52357826M');
        $this->assertEquals($paciente_updated->slug, $paciente->slug);
    }

    /** @test */
    public function delete_api_paciente()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $paciente = Paciente::latest('created_at')->first();

        $response = $this->delete('/api/pacientes/'. $paciente->slug);

        $response->assertOk();

        $paciente_deleted = Paciente::where('slug', $paciente->slug)->first();

        $this->assertEmpty($paciente_deleted);
    }

    /** @test */
    public function get_api_pacientes()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        Paciente::factory()->count(3)->create();

        $response = $this->get('/api/pacientes');

        $response->assertOk();
    }

    /** @test */
    public function get_api_paciente()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        $paciente = Paciente::factory()->create();
        Diagnostico::factory()->count(3)->create();

        $response = $this->get('/api/pacientes/'.$paciente->slug);

        $response->assertOk();
    }

    /** @test */
    public function create_api_paciente()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $response = $this->post('/api/pacientes', [
            'nombres' => 'creatednombres',
            'apellidos' => 'createdapellidos',
            'dni' => '52357826M',
        ]);

        $response->assertOk();

        $paciente = Paciente::latest('created_at')->first();

        $this->assertEquals($paciente->nombres, 'creatednombres');
        $this->assertEquals($paciente->apellidos, 'createdapellidos');
        $this->assertEquals($paciente->dni, '52357826M');
        $this->assertEquals($paciente->user_id, $user->id);
    }
}
