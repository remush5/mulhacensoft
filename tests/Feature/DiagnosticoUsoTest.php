<?php

namespace Tests\Feature;

use App\Models\Diagnostico;
use App\Models\Paciente;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DiagnosticoUsoTest extends TestCase
{
    /** @test */
    public function create_diagnostico()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        $paciente = Paciente::factory()->create();

        $response = $this->post('/diagnosticos', [
            'descripcion' => 'createdescripcion',
            'paciente_slug' => $paciente->slug,
        ]);

        $response->assertOk();

        $diagnostico = Diagnostico::where('paciente_slug', $paciente->slug)->latest('created_at')->first();

        $this->assertEquals($diagnostico->descripcion, 'createdescripcion');
        $this->assertEquals($diagnostico->paciente_slug, $paciente->slug);
    }


    /** @test */
    public function update_diagnostico()
    {
        $this->withoutExceptionHandling();

        $diagnostico = Diagnostico::latest('created_at')->first();

        $response = $this->put('/diagnosticos/'. $diagnostico->slug, [
            'descripcion' => 'updatedescripcion',
            'slug' => $diagnostico->slug,
        ]);

        $response->assertOk();

        $diagnostico_updated = Diagnostico::latest('updated_at')->first();

        $this->assertEquals($diagnostico_updated->descripcion, 'updatedescripcion');
        $this->assertEquals($diagnostico_updated->slug, $diagnostico->slug);
    }

    /** @test */
    public function delete_diagnostico()
    {
        $this->withoutExceptionHandling();

        $diagnostico = Diagnostico::latest('created_at')->first();

        $response = $this->delete('/diagnosticos/'. $diagnostico->slug);

        $response->assertOk();

        $diagnostico_deleted = Paciente::where('slug', $diagnostico->slug)->first();

        $this->assertEmpty($diagnostico_deleted);
    }


    /** @test */
    public function create_api_diagnostico()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        $paciente = Paciente::factory()->create();

        $response = $this->post('/api/diagnosticos', [
            'descripcion' => 'createdescripcion',
            'paciente_slug' => $paciente->slug,
        ]);

        $response->assertOk();

        $diagnostico = Diagnostico::where('paciente_slug', $paciente->slug)->latest('created_at')->first();

        $this->assertEquals($diagnostico->descripcion, 'createdescripcion');
        $this->assertEquals($diagnostico->paciente_slug, $paciente->slug);
    }


    /** @test */
    public function update_api_diagnostico()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        $diagnostico = Diagnostico::latest('created_at')->first();

        $response = $this->put('/api/diagnosticos/'. $diagnostico->slug, [
            'descripcion' => 'updatedescripcion',
        ]);

        $response->assertOk();

        /*$diagnostico_updated = Diagnostico::latest('updated_at')->first();

        $this->assertEquals($diagnostico_updated->descripcion, 'updatedescripcion');
        $this->assertEquals($diagnostico_updated->slug, $diagnostico->slug);*/
    }

    /** @test */
    public function delete_api_diagnostico()
    {
        $this->withoutExceptionHandling();

        $user = User::first();

        if($user == null){
            $user = User::factory()->create();
        }

        $this->actingAs($user, 'api');

        $diagnostico = Diagnostico::latest('created_at')->first();

        $response = $this->delete('/api/diagnosticos/'. $diagnostico->slug);

        $response->assertOk();

        $diagnostico_deleted = Paciente::where('slug', $diagnostico->slug)->first();

        $this->assertEmpty($diagnostico_deleted);
    }

}
