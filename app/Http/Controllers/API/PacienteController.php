<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Models\Diagnostico;
use App\Models\Paciente;
use App\Http\Controllers\PacienteController as WebPacienteController;
use Validator;
use Illuminate\Http\Request;
use App\Http\Resources\Paciente as PacienteResource;
use App\Http\Resources\Diagnostico as DiagnosticoResource;
use Auth;

class PacienteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacientes = Paciente::where('user_id', Auth::user()->id)->get();
        return $this->sendResponse(PacienteResource::collection($pacientes), 'Pacientes.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombres' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'apellidos' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'dni' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Error de validacion.', $validator->errors());
        }

        if(!WebPacienteController::check_dni($request->input('dni'))) {
            return $this->sendError('Error de validacion.', ['errors' => ['dni' => ['DNI erroneo']]]);
        }

        $paciente = new Paciente([
            'nombres' => $request->input('nombres'),
            'apellidos' => $request->input('apellidos'),
            'dni' => $request->input('dni'),
            'slug' => md5(uniqid(rand(), true)),
            'user_id' => Auth::user()->id,
        ]);
        $paciente->save();

        return $this->sendResponse(new PacienteResource($paciente), 'Paciente creado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::where('slug', $id)->first();
        $diagnosticos = Diagnostico::where('paciente_slug', $id)->get();
        return $this->sendResponse(['diagnosticos' => DiagnosticoResource::collection($diagnosticos), 'paciente' => new PacienteResource($paciente)], 'Paciente y diagnostico.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombres' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'apellidos' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'dni' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Error de validacion.', $validator->errors());
        }

        if(!WebPacienteController::check_dni($request->input('dni'))) {
            return $this->sendError('Error de validacion.', ['errors' => ['dni' => ['DNI erroneo']]]);
        }

        $paciente = Paciente::where('slug', $id)->first();
        $paciente->update([
            'nombres' => $request->input('nombres'),
            'apellidos' => $request->input('apellidos'),
            'dni' => $request->input('dni')
        ]);
        return $this->sendResponse(new PacienteResource($paciente), 'Paciente actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paciente = Paciente::where('slug', $id)->first();
        $paciente->delete();

        return $this->sendResponse([], 'Paciente borrado.');
    }
}
