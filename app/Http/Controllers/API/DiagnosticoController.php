<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\Paciente as PacienteResource;
use App\Models\Diagnostico;
use App\Models\Paciente;
use Validator;
use Illuminate\Http\Request;
use App\Http\Resources\Diagnostico as DiagnosticoResource;

class DiagnosticoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
        ]);

        if($validator->fails()){
            return $this->sendError('Error de validacion.', $validator->errors());
        }

        $diagnostico = new Diagnostico([
            'descripcion' => $request->input('descripcion'),
            'slug' => md5(uniqid(rand(), true)),
            'paciente_slug' => $request->input('paciente_slug'),

        ]);
        $diagnostico->save();

        return $this->sendResponse(new DiagnosticoResource($diagnostico), 'Diagnostico creado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $diagnostico = Diagnostico::where('slug', $id)->first();
        return $this->sendResponse(new DiagnosticoResource($diagnostico), 'Diagnostico.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
        ]);

        if($validator->fails()){
            return $this->sendError('Error de validacion.', $validator->errors());
        }

        $diagnostico = Diagnostico::where('slug', $id)->first();
        $diagnostico->update($request->all());

        return $this->sendResponse(new DiagnosticoResource($diagnostico), 'Diagnostico actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diagnostico = Diagnostico::where('slug', $id)->first();
        $diagnostico->delete();

        return $this->sendResponse([], 'Diagnostico borrado.');
    }
}
