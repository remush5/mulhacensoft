<?php

namespace App\Http\Controllers;

use App\Models\Diagnostico;
use App\Models\Paciente;
use Illuminate\Http\Request;

class DiagnosticoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'descripcion' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
        ]);

        $diagnostico = new Diagnostico([
            'descripcion' => $request->input('descripcion'),
            'slug' => md5(uniqid(rand(), true)),
            'paciente_slug' => $request->input('paciente_slug'),

        ]);
        $diagnostico->save();

        return response()->json($diagnostico->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'descripcion' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
        ]);

        $diagnostico = Diagnostico::where('slug', $id)->first();
        $diagnostico->update($request->all());

        return response()->json('Diagnostico actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diagnostico = Diagnostico::where('slug', $id)->first();
        $diagnostico->delete();

        return response()->json('Diagnostico borrado!');
    }
}
