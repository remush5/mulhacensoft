<?php

namespace App\Http\Controllers;

use App\Models\Diagnostico;
use Illuminate\Http\Request;
use App\Models\Paciente;
use Auth;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombres' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'apellidos' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'dni' => 'required',
        ]);

        if(!self::check_dni($request->input('dni'))) {
            return response()->json(['errors' => ['dni' => ['DNI erroneo']]],'422',);
        }

        $paciente = new Paciente([
            'nombres' => $request->input('nombres'),
            'apellidos' => $request->input('apellidos'),
            'dni' => $request->input('dni'),
            'slug' => md5(uniqid(rand(), true)),
            'user_id' => Auth::user()->id,

        ]);
        $paciente->save();

        return response()->json($paciente->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::where('slug', $id)->first();
        $diagnosticos = Diagnostico::where('paciente_slug', $id)->get();
        return view('paciente')->with(['diagnosticos' => $diagnosticos, 'paciente' => $paciente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombres' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'apellidos' => 'required|max:70|regex:/^[A-Za-z-_\s]+$/',
            'dni' => 'required',
        ]);

        if(!self::check_dni($request->input('dni'))) {
            return response()->json(['errors' => ['dni' => ['DNI erroneo']]],'422',);
        }

        $paciente = Paciente::where('slug', $id)->first();
        $paciente->update([
                'nombres' => $request->input('nombres'),
                'apellidos' => $request->input('apellidos'),
                'dni' => $request->input('dni')
        ]);

        return response()->json('Paciente actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paciente = Paciente::where('slug', $id)->first();
        $paciente->delete();

        return response()->json('Paciente borrado!');
    }

    public static function check_dni($cif)
    {
        $cif = strtoupper($cif);

        for ($i = 0; $i < 9; $i ++) {
            $num[$i] = substr($cif, $i, 1);
        }

        // Invalid format
        if (!preg_match('/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/', $cif)) {
            return false;
        }

        // NIFs
        if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $cif)) {
            return ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 0, 8) % 23, 1));
        }

        if (preg_match('/^[XYZ]{1}/', $cif)) {
            return ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array('X','Y','Z'), array('0','1','2'), $cif), 0, 8) % 23, 1));
        }

        $suma = $num[2] + $num[4] + $num[6];
        for ($i = 1; $i < 8; $i += 2) {
            $suma += array_sum(str_split(2 * $num[$i]));
        }
        $n = 10 - substr($suma, strlen($suma) - 1, 1);

        // Special NIFs
        if (preg_match('/^[KLM]{1}/', $cif)) {
            return ($num[8] == chr(64 + $n) || $num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 1, 8) % 23, 1));
        }

        // CIFs
        if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $cif)) {
            return ($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1));
        }

        return false;
    }
}
