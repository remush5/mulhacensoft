<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Diagnostico extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'descripcion' => $this->descripcion,
            'paciente_slug' => $this->paciente_slug,
            'slug' => $this->slug,
            'created_at' => $this->created_at->format('d/m/Y'),
        ];
    }
}
