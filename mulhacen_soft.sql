-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-06-2021 a las 11:50:46
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mulhacen_soft`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnosticos`
--

CREATE TABLE `diagnosticos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paciente_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `diagnosticos`
--

INSERT INTO `diagnosticos` (`id`, `descripcion`, `slug`, `paciente_slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(22, 'updatedescripcion', '974fd6fef87d65cfcd41df6b5ba2952a', 'fdbdb14cd609555a76130dd12f8fdaac', '2021-06-23 17:05:44', '2021-06-23 17:05:52', '2021-06-23 17:05:52'),
(23, 'testadescripcion', '7d02ac4e0592d31d11eaf7b1e26ab4cd', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-23 17:09:03', '2021-06-23 17:09:03', NULL),
(24, 'testadescripcion', '963c9df41acb9796ae3aabda2716b140', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-23 17:09:03', '2021-06-23 17:09:03', NULL),
(25, 'testadescripcion', '38468891a2202dce3854c2b00842753f', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-23 17:09:03', '2021-06-23 17:09:03', NULL),
(26, 'updatedescripcion', 'c4638dc88435c72ca5fe8c461dc07fd1', '70e2f5a03c087f35a6fd5e5370b47695', '2021-06-23 17:09:46', '2021-06-23 17:09:46', '2021-06-23 17:09:46'),
(27, 'testadescripcion', 'a6fc2c37aeac81bd40603bad9e31a2fe', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:07', '2021-06-25 07:31:07', NULL),
(28, 'testadescripcion', '9854ce29e45426ce4e298bee3db838a0', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:07', '2021-06-25 07:31:07', NULL),
(29, 'testadescripcion', 'd1c57281ce8f1b246ba7b18ce8b578c5', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:07', '2021-06-25 07:31:07', NULL),
(30, 'testadescripcion', 'b8449cc9c2e3f6619ff975e4d02bdec2', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:07', '2021-06-25 07:31:07', NULL),
(31, 'testadescripcion', 'eebafeb272e99779d2dea946c831e002', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:07', '2021-06-25 07:31:07', NULL),
(32, 'testadescripcion', '3e243c3aa3b6087e89a09476dd6e80da', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:07', '2021-06-25 07:31:07', NULL),
(33, 'testadescripcion', '738e3392e1d7b93d7e317bc560b093c4', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:55', '2021-06-25 07:31:55', NULL),
(34, 'testadescripcion', 'e725f2644830f4fc2a6a3f1d66a3c424', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:55', '2021-06-25 07:31:55', NULL),
(35, 'testadescripcion', 'b0c8bf18a0f8c4571d08d736fd1d3d20', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:55', '2021-06-25 07:31:55', NULL),
(36, 'testadescripcion', '734471d244b91eb057b674d1387f7c9f', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:55', '2021-06-25 07:31:55', NULL),
(37, 'testadescripcion', '73c3cc230a391954c5e9b0119227d540', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:55', '2021-06-25 07:31:55', NULL),
(38, 'testadescripcion', '9085aa89b83d3a8be92c0bf8de775b3b', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:31:55', '2021-06-25 07:31:55', NULL),
(39, 'testadescripcion', '7891c5c3a697729e17da79d60aaeed98', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:32:56', '2021-06-25 07:32:56', NULL),
(40, 'testadescripcion', 'b72da86d7d929b059a12ab03711c230d', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:32:56', '2021-06-25 07:32:56', NULL),
(41, 'testadescripcion', '2dc6426452306b97509960ab6f02cd71', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:32:56', '2021-06-25 07:32:56', NULL),
(42, 'testadescripcion', '2e4c69d7b93b1bd6a60a096541558daf', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:32:56', '2021-06-25 07:32:56', NULL),
(43, 'testadescripcion', '7eb73e0bebc1d23030fa45388898a8c4', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:32:56', '2021-06-25 07:32:56', NULL),
(44, 'testadescripcion', 'bf5ec21647a0db71fa6dddd0698bc771', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:32:56', '2021-06-25 07:32:56', NULL),
(45, 'testadescripcion', 'da792c28976f27300e3b6a56ecb0a9b5', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:34:03', '2021-06-25 07:34:03', NULL),
(46, 'testadescripcion', 'c0ca9e0443afa9be4dd34ab8b7e5c2c5', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:34:03', '2021-06-25 07:34:03', NULL),
(47, 'testadescripcion', 'bdf9e896191dcd776295d4fefce844c1', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:34:03', '2021-06-25 07:34:03', NULL),
(48, 'testadescripcion', '44a77177e34ae8c04e579ff14ba7736d', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:34:03', '2021-06-25 07:34:03', NULL),
(49, 'testadescripcion', '849fed385b8145c189d7796c3f62b744', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:34:03', '2021-06-25 07:34:03', NULL),
(50, 'testadescripcion', '2d1e59b9c90c0c293ce58ac1bec88d2e', 'f25d9d1bc6a623a86d5561d2b8aaae4b', '2021-06-25 07:34:03', '2021-06-25 07:34:03', NULL),
(51, 'testadescripcion', 'c0e0f586ca555c6cb87ca9550c565ddb', 'f59dfb5123110d167b0ceabbe7f6292e', '2021-06-25 07:34:53', '2021-06-25 07:34:53', NULL),
(52, 'testadescripcion', '521608f941b8cc70ec0077de88d8b9de', 'f59dfb5123110d167b0ceabbe7f6292e', '2021-06-25 07:34:53', '2021-06-25 07:34:53', NULL),
(53, 'testadescripcion', 'a2fa7227f7e2f17820d5ddbbdfb5554f', 'f59dfb5123110d167b0ceabbe7f6292e', '2021-06-25 07:34:53', '2021-06-25 07:34:53', NULL),
(54, 'testadescripcion', '07c5f2f29e3a2252339ab4e7071e22e2', 'f59dfb5123110d167b0ceabbe7f6292e', '2021-06-25 07:34:53', '2021-06-25 07:34:53', NULL),
(55, 'testadescripcion', '54a6773ba365488d0e0278336f6ea491', 'f59dfb5123110d167b0ceabbe7f6292e', '2021-06-25 07:34:53', '2021-06-25 07:34:53', NULL),
(56, 'testadescripcion', '984900da7c4ef0244018ae2cf7ed774c', 'f59dfb5123110d167b0ceabbe7f6292e', '2021-06-25 07:34:53', '2021-06-25 07:34:53', NULL),
(57, 'testadescripcion', '57bed955019f5570c1597290179df8da', '6b19865e6f20d605455af6315d95f45e', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(58, 'testadescripcion', '331cab64fd234a4c3910b76561dff589', '6b19865e6f20d605455af6315d95f45e', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(59, 'testadescripcion', 'bcd2bd99ff848552f0e92fd3eed3f0b1', '6b19865e6f20d605455af6315d95f45e', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(60, 'testadescripcion', '280e33830ec88b1c8f41070ec0b7c8db', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(61, 'testadescripcion', 'fae803662b255155953a66ab9b646d92', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(62, 'testadescripcion', '8141e84f36925606d16680abbf13b7e7', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(63, 'updatedescripcion', 'c638250ff3ea8093f80984d0072c1a09', '2cb8cc06fa5e1e37736067d3d380b0f7', '2021-06-25 07:38:03', '2021-06-25 07:38:03', '2021-06-25 07:38:03'),
(64, 'updatedescripcion', '94113509cb6a50661a5c8aae4f57c095', '667aa63220b83390fa1177b644d44af2', '2021-06-25 07:38:03', '2021-06-25 07:46:03', NULL),
(65, 'updatedescripcion', '61c16f06d2add56516d7818453d9c277', 'a93f711f76f1d136642b38e11d466018', '2021-06-25 07:38:35', '2021-06-25 07:38:35', '2021-06-25 07:38:35'),
(66, 'updatedescripcion', 'c104634d61f352ef568a165445de3b32', '59e2345f417f1ac5049086000bfdc18e', '2021-06-25 07:38:35', '2021-06-25 07:38:35', '2021-06-25 07:38:35'),
(67, 'updatedescripcion', '91b48cc4d5fed541162c16015390e5b7', 'ff71b91abe7ad91c9bceacc85894edd8', '2021-06-25 07:47:21', '2021-06-25 07:47:21', '2021-06-25 07:47:21'),
(68, 'updatedescripcion', '90f887dcb46b7c4ed9cadd3105a0f9f3', '0b0e0682448350833a306ad72214214c', '2021-06-25 07:47:21', '2021-06-25 07:47:21', '2021-06-25 07:47:21'),
(69, 'testadescripcion', 'bea3e3ba571e5d0d4d8fc92c2e8f1fba', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(70, 'testadescripcion', 'bbff4bf8924b8a5212703f6606f42e0c', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(71, 'testadescripcion', '1269af54c8477f97baa63618fc01e6e4', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(72, 'testadescripcion', '64df627c4e948bd6d11daf4e232317ec', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(73, 'testadescripcion', '728413d2dfb5c4f8807a1c5e46287c14', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(74, 'testadescripcion', 'e39736c3847b7064d91b72b3a6f6f155', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(75, 'testadescripcion', '9888256ca274a4aa64ad37b41853c9f3', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(76, 'testadescripcion', '6fd8fbf3035e86d48762a2d751602eb2', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(77, 'testadescripcion', '7006fda333b282bcb777c5d79d2aaf59', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(78, 'testadescripcion', '16ae3693720a77b7b947db42d631dadc', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(79, 'testadescripcion', '2d58155fc55f24ae0bce936be8b07a53', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(80, 'testadescripcion', '9453e02e8d375cfaf018453c91d5d598', 'faa11c9e1836da2ca880c53d7c7527cd', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2021_06_21_094646_create_pacientes_table', 1),
(15, '2021_06_21_094700_create_diagnosticos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombres` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`id`, `nombres`, `apellidos`, `dni`, `slug`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(106, 'updatednombres', 'updatedapellidos', '52357826M', 'd039f45c0e353b29883a1e9c56c0976c', '31', '2021-06-25 07:35:11', '2021-06-25 07:35:11', '2021-06-25 07:35:11'),
(107, 'updatednombres', 'updatedapellidos', '52357826M', '6b19865e6f20d605455af6315d95f45e', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', '2021-06-25 07:35:11'),
(108, 'testnombres', 'testapellidos', 'testdni', 'faa11c9e1836da2ca880c53d7c7527cd', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(109, 'testnombres', 'testapellidos', 'testdni', '997b2b5994e23c00b8a5bf49a8345c93', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(110, 'testnombres', 'testapellidos', 'testdni', '87ad36b54a9f841a89288ac5d1f6947b', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(111, 'creatednombres', 'createdapellidos', '52357826M', '2f343c923c58c2b03893d07686d2f777', '32', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(112, 'testnombres', 'testapellidos', 'testdni', 'b0bab7e9207d33920ccb347a2852cb68', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(113, 'testnombres', 'testapellidos', 'testdni', '2cfcd6a5aa385fb0e22c379824ea1c2d', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(114, 'testnombres', 'testapellidos', 'testdni', 'b942dbe1be2bdf1aaabb52087ff4b973', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(115, 'testnombres', 'testapellidos', 'testdni', '5b41ad7c71da3cb1abc560828733a985', '1', '2021-06-25 07:35:11', '2021-06-25 07:35:11', NULL),
(116, 'creatednombres', 'createdapellidos', '52357826M', '9d8b28697cd151e970835f5d2610d2b9', '35', '2021-06-25 07:35:40', '2021-06-25 07:35:40', NULL),
(117, 'testnombres', 'testapellidos', 'testdni', '2cb8cc06fa5e1e37736067d3d380b0f7', '1', '2021-06-25 07:38:03', '2021-06-25 07:38:03', NULL),
(118, 'testnombres', 'testapellidos', 'testdni', '667aa63220b83390fa1177b644d44af2', '1', '2021-06-25 07:38:03', '2021-06-25 07:38:03', NULL),
(119, 'testnombres', 'testapellidos', 'testdni', 'a93f711f76f1d136642b38e11d466018', '1', '2021-06-25 07:38:35', '2021-06-25 07:38:35', NULL),
(120, 'testnombres', 'testapellidos', 'testdni', '59e2345f417f1ac5049086000bfdc18e', '1', '2021-06-25 07:38:35', '2021-06-25 07:38:35', NULL),
(121, 'testnombres', 'testapellidos', 'testdni', 'ff71b91abe7ad91c9bceacc85894edd8', '1', '2021-06-25 07:47:21', '2021-06-25 07:47:21', NULL),
(122, 'testnombres', 'testapellidos', 'testdni', '0b0e0682448350833a306ad72214214c', '1', '2021-06-25 07:47:21', '2021-06-25 07:47:21', NULL),
(123, 'updatednombres', 'updatedapellidos', '52357826M', '486be682dfe51037533bb32bced3bc9b', '36', '2021-06-25 07:48:07', '2021-06-25 07:48:08', '2021-06-25 07:48:08'),
(124, 'updatednombres', 'updatedapellidos', '52357826M', '32f8c6e141d6c4949a261a15fc40d1f7', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', '2021-06-25 07:48:08'),
(125, 'testnombres', 'testapellidos', 'testdni', 'f375a298709a3475e232d677094688e4', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(126, 'testnombres', 'testapellidos', 'testdni', 'ef5099f19a476d4a30db16ebe050fadf', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(127, 'testnombres', 'testapellidos', 'testdni', 'fce7b1586fdcf0200e705d5c58ba2bec', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(128, 'creatednombres', 'createdapellidos', '52357826M', '48b330a06185bbc2a511f3cf039b23cb', '37', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(129, 'testnombres', 'testapellidos', 'testdni', '4b4d234dd84948c8495bc106b3211e44', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(130, 'testnombres', 'testapellidos', 'testdni', '3f92ca298052c426631afb3159ab0997', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(131, 'testnombres', 'testapellidos', 'testdni', 'e973273fac71360a5dd4d99865620be5', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(132, 'testnombres', 'testapellidos', 'testdni', '151e1a0ea676f225528f197b99faf0dc', '1', '2021-06-25 07:48:08', '2021-06-25 07:48:08', NULL),
(133, 'updatednombres', 'updatedapellidos', '52357826M', 'b363786900c58c3eca98edeaac17ee3e', '40', '2021-06-25 07:50:03', '2021-06-25 07:50:03', '2021-06-25 07:50:03'),
(134, 'updatednombres', 'updatedapellidos', '52357826M', '808c121b68ca5fb9fe8e4d43f091c5ff', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', '2021-06-25 07:50:04'),
(135, 'testnombres', 'testapellidos', 'testdni', '8d8acec9ca5fd503904906b22dbe37ca', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(136, 'testnombres', 'testapellidos', 'testdni', '90d3ae2f63a2065f8c4d8cc92c2e892c', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(137, 'testnombres', 'testapellidos', 'testdni', '04a4ec85b8d7ef1955e55659e5e9f71d', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(138, 'testnombres', 'testapellidos', 'testdni', '42ffca06ce1f7cf7e21aeaa1fb2d4000', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(139, 'testnombres', 'testapellidos', 'testdni', '583f478bf3ca749e5cfaefcfb63acc74', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(140, 'testnombres', 'testapellidos', 'testdni', '26415161113d8a4626046121561e82aa', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(141, 'testnombres', 'testapellidos', 'testdni', 'b89cde51ce5ab515caea9f21dc94195d', '1', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(142, 'creatednombres', 'createdapellidos', '52357826M', '85266a732e2e33307f5db2e0dee7ad4a', '43', '2021-06-25 07:50:04', '2021-06-25 07:50:04', NULL),
(143, 'creatednombres', 'createdapellidos', '52357826M', 'a52647c9b202e9a64c873f6f2b7551d1', '44', '2021-06-25 07:50:16', '2021-06-25 07:50:16', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Remus Hurmuz', 'remushurmuz4@gmail.com', NULL, '$2y$10$/mSpk4ecIsALP.8oHsM/T.OPMFuFC3R22.Y7rUQDH729QHR5FKgp6', '6jp7tX3I9FcGqYktdgaHQnunH4cLTnvmLMIfQkEJHJeBNeGfpq4UymL9dv5z', '2021-06-21 09:24:06', '2021-06-21 09:24:06'),
(5, 'Emanuel Beer', 'carolyne21@example.org', '2021-06-23 17:08:39', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KgY8J1JgAI', '2021-06-23 17:08:39', '2021-06-23 17:08:39'),
(6, 'Dr. Loy Quitzon MD', 'julia.gerlach@example.net', '2021-06-23 17:09:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DSW1q8sNti', '2021-06-23 17:09:03', '2021-06-23 17:09:03'),
(7, 'Nathanael Sporer', 'okutch@example.org', '2021-06-25 07:22:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'F9VLXezXHZ', '2021-06-25 07:22:52', '2021-06-25 07:22:52'),
(8, 'Mr. Joesph Pfeffer III', 'xwilkinson@example.com', '2021-06-25 07:23:42', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'gBwTYxupLE', '2021-06-25 07:23:42', '2021-06-25 07:23:42'),
(9, 'Mrs. Sydnee Lueilwitz', 'sierra85@example.com', '2021-06-25 07:23:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IPfpSd5eNb', '2021-06-25 07:23:56', '2021-06-25 07:23:56'),
(10, 'Jermaine Hoeger', 'hickle.paige@example.net', '2021-06-25 07:25:29', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '35rJ0kMvNh', '2021-06-25 07:25:29', '2021-06-25 07:25:29'),
(11, 'Prof. Karlie Harris', 'ejohnston@example.net', '2021-06-25 07:28:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4eUhmkXURp', '2021-06-25 07:28:26', '2021-06-25 07:28:26'),
(12, 'Angus Gottlieb', 'colleen70@example.com', '2021-06-25 07:29:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DxRtaec9Wh', '2021-06-25 07:29:15', '2021-06-25 07:29:15'),
(13, 'Rogers Bosco', 'jakubowski.austin@example.org', '2021-06-25 07:31:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'BXWXp6vzt0', '2021-06-25 07:31:07', '2021-06-25 07:31:07'),
(14, 'Robbie Schaden', 'cassandra04@example.com', '2021-06-25 07:31:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dL298SxAGS', '2021-06-25 07:31:07', '2021-06-25 07:31:07'),
(15, 'Ms. Orpha Moen Sr.', 'dubuque.joana@example.com', '2021-06-25 07:31:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'OC3YrIEcHz', '2021-06-25 07:31:55', '2021-06-25 07:31:55'),
(16, 'Wendell Bartell', 'steve.reynolds@example.net', '2021-06-25 07:31:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yz9K8OghFU', '2021-06-25 07:31:55', '2021-06-25 07:31:55'),
(17, 'Dr. Jessie Treutel PhD', 'ben.upton@example.com', '2021-06-25 07:31:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jMk25t1810', '2021-06-25 07:31:55', '2021-06-25 07:31:55'),
(18, 'Prof. Milton Harber DVM', 'gleason.adriel@example.net', '2021-06-25 07:31:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '53LefjRAsZ', '2021-06-25 07:31:55', '2021-06-25 07:31:55'),
(19, 'Mya Beahan', 'elaina55@example.com', '2021-06-25 07:32:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cufoWUedXq', '2021-06-25 07:32:56', '2021-06-25 07:32:56'),
(20, 'Miss Lia Bernhard', 'schaden.brayan@example.com', '2021-06-25 07:32:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'oRBx84ZOxs', '2021-06-25 07:32:56', '2021-06-25 07:32:56'),
(21, 'Ceasar Christiansen DDS', 'natasha54@example.com', '2021-06-25 07:32:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DDPdj3rAlw', '2021-06-25 07:32:56', '2021-06-25 07:32:56'),
(22, 'Prof. Kale Pouros II', 'lucius55@example.com', '2021-06-25 07:32:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KQagFSBRmT', '2021-06-25 07:32:56', '2021-06-25 07:32:56'),
(23, 'Lisa Crona', 'micaela.ortiz@example.net', '2021-06-25 07:34:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'OSblFhRS2K', '2021-06-25 07:34:03', '2021-06-25 07:34:03'),
(24, 'Mrs. Vergie Smitham IV', 'langosh.eudora@example.net', '2021-06-25 07:34:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ws0kvcbSEU', '2021-06-25 07:34:03', '2021-06-25 07:34:03'),
(25, 'Kaci Kuhn', 'beulah53@example.org', '2021-06-25 07:34:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'nWwVriXL8J', '2021-06-25 07:34:03', '2021-06-25 07:34:03'),
(26, 'Marta Schmeler', 'dorothea76@example.com', '2021-06-25 07:34:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ezHdBVciWV', '2021-06-25 07:34:03', '2021-06-25 07:34:03'),
(27, 'Ludwig Predovic', 'uswaniawski@example.com', '2021-06-25 07:34:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'u9Cgbu8FQV', '2021-06-25 07:34:53', '2021-06-25 07:34:53'),
(28, 'Mr. Julien Eichmann III', 'nikolaus.gage@example.org', '2021-06-25 07:34:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'crt4UbH0VU', '2021-06-25 07:34:53', '2021-06-25 07:34:53'),
(29, 'Bettie Gaylord', 'raynor.zoie@example.net', '2021-06-25 07:34:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wQPPMF6uQB', '2021-06-25 07:34:53', '2021-06-25 07:34:53'),
(30, 'Mrs. Rosalia Schinner', 'botsford.ida@example.com', '2021-06-25 07:34:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'B9lHjFKKzx', '2021-06-25 07:34:53', '2021-06-25 07:34:53'),
(31, 'Ollie Ankunding II', 'schoen.amya@example.com', '2021-06-25 07:35:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aL9fzS8U4L', '2021-06-25 07:35:11', '2021-06-25 07:35:11'),
(32, 'Prudence Schroeder I', 'elizabeth19@example.org', '2021-06-25 07:35:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YWWMDM1Mio', '2021-06-25 07:35:11', '2021-06-25 07:35:11'),
(33, 'Florence Mosciski', 'zbarton@example.net', '2021-06-25 07:35:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PhJRscHl1l', '2021-06-25 07:35:11', '2021-06-25 07:35:11'),
(34, 'Nathan Rice', 'wilfred22@example.net', '2021-06-25 07:35:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'A5YeLCw7QY', '2021-06-25 07:35:11', '2021-06-25 07:35:11'),
(35, 'Mariela Greenholt III', 'hoppe.ibrahim@example.com', '2021-06-25 07:35:40', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '34vIsQizcB', '2021-06-25 07:35:40', '2021-06-25 07:35:40'),
(36, 'Antonette Pagac', 'jordan13@example.net', '2021-06-25 07:48:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1QnGXS5wCD', '2021-06-25 07:48:07', '2021-06-25 07:48:07'),
(37, 'America Gutkowski', 'fred.paucek@example.org', '2021-06-25 07:48:08', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6bEByWv0BX', '2021-06-25 07:48:08', '2021-06-25 07:48:08'),
(38, 'Itzel Cormier', 'bert.runolfsdottir@example.com', '2021-06-25 07:48:08', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GfAaTojSkb', '2021-06-25 07:48:08', '2021-06-25 07:48:08'),
(39, 'Ulices Donnelly', 'sarai.quitzon@example.com', '2021-06-25 07:48:08', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'noydFvhO4w', '2021-06-25 07:48:08', '2021-06-25 07:48:08'),
(40, 'Geovanni Mueller', 'justen34@example.org', '2021-06-25 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'hMwUBR55DT', '2021-06-25 07:50:03', '2021-06-25 07:50:03'),
(41, 'Chanelle Klocko', 'hkuvalis@example.net', '2021-06-25 07:50:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FqYo3naEOh', '2021-06-25 07:50:04', '2021-06-25 07:50:04'),
(42, 'Kristina Smith', 'oconroy@example.org', '2021-06-25 07:50:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 's2WGJS7jra', '2021-06-25 07:50:04', '2021-06-25 07:50:04'),
(43, 'Mr. Porter Mann', 'goodwin.blaze@example.com', '2021-06-25 07:50:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LRAFCcqQdF', '2021-06-25 07:50:04', '2021-06-25 07:50:04'),
(44, 'Mrs. Lily Jakubowski Sr.', 'cecelia70@example.com', '2021-06-25 07:50:16', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dDMLLhwpuZ', '2021-06-25 07:50:16', '2021-06-25 07:50:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `diagnosticos`
--
ALTER TABLE `diagnosticos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `diagnosticos`
--
ALTER TABLE `diagnosticos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
