@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <paciente-component :paciente="{{$paciente}}" :diagnosticos="{{$diagnosticos}}"></paciente-component>
        </div>
    </div>
@endsection
